import 'dart:io';

import 'package:edge_detection/edge_detection.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class EdgeDetectionExample extends StatefulWidget {
  const EdgeDetectionExample({super.key});

  @override
  _EdgeDetectionExampleState createState() => _EdgeDetectionExampleState();
}

class _EdgeDetectionExampleState extends State<EdgeDetectionExample> {
  String? _scannedImgPath;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 8),
        ElevatedButton.icon(
          icon: const Icon(Icons.camera),
          label: const Text('start'),
          onPressed: _doScan,
        ),
        if (_scannedImgPath != null) ...[
          Text(_scannedImgPath!),
          Expanded(
            child: Image.file(File(_scannedImgPath!)),
          )
        ]
      ],
    );
  }

  Future<void> _doScan() async {

    bool isCameraGranted = await Permission.camera.request().isGranted;
    if (!isCameraGranted) {
      isCameraGranted = await Permission.camera.request() == PermissionStatus.granted;
    }

    if (!isCameraGranted) {
      // Have not permission to camera
      return;
    }

    /// This [detectEdge] is the only method exposed by the plugin, it'll open
    /// the camera and do the scanning.
    /// !Unfortunately we cannot customize the behavior like loading image from
    /// !gallery or changing the saved image path.
    String imagePath = join((await getApplicationSupportDirectory()).path,
        "${(DateTime.now().millisecondsSinceEpoch / 1000).round()}.jpeg");

    bool isSuccess = await EdgeDetection.detectEdge(imagePath);
    if (isSuccess) {
      setState(() => _scannedImgPath = imagePath);
    }
  }
}
