import 'package:flutter/widgets.dart';

class MyHelpers {
  static final MyHelpers _singleton = MyHelpers._internal();

  factory MyHelpers() {
    return _singleton;
  }

  MyHelpers._internal() {
    // initialization code
  }

  void safeMount(State state, VoidCallback callback) {
    if (!state.mounted) {
      return;
    }
    callback();
  }

  // other class methods and properties
}
